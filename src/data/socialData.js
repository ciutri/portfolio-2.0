export const socialsData = {
    github: '',
    facebook: 'https://www.facebook.com/ndttri.2203',
    linkedIn: 'https://www.linkedin.com/in/tri-nguyen-80806a206/',
    instagram: 'https://www.instagram.com/urus.lewis/',
    codepen: '',
    twitter: '',
    reddit: '',
    blogger: '',
    medium: 'https://medium.com/@sreerag.rajan5',
    stackOverflow: '',
    gitlab: '',
    youtube: '',
    bitbucket: 'https://bitbucket.org/urusng',
}