export const aboutData = {
    title: "Who I am",
    description1: "Hello! My name is Nguyen Duong Thanh Tri (Taric Nguyen). I'm an aspiring project manager based in Vietnam.",
    description2: "I am a student at CTUET.",
    description3: "With five years of experience as BE Developer (Python, Golang, Nodejs). I have used these languages and frameworks to create many reliable, secure, and extensible projects. I also have three years of experience as a Project Owner, overseeing the entire development process from planning to implementation." +

        "I have strong leadership and communication skills that allow me to collaborate effectively with clients and team members. I am always eager to learn new technologies and apply them to solve real-world problems.",
    image: 2
}