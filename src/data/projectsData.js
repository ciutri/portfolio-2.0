import portfolio from '../assets/svg/projects/portfolio.png'
export const projectsData = [
    {
        id: 1,
        projectName: 'Portfolio',
        projectDesc: 'My Portfolio',
        tags: ['React'],
        code: 'https://bitbucket.org/urusng/portfolio/src/master/',
        demo: 'https://portfolio-urusng.vercel.app/',
        image: portfolio
    },
    {
        id: 2,
        projectName: 'Video Call',
        projectDesc: 'The video call system. Coming soon after deploy success',
        tags: ['React', 'Golang'],
        code: '',
        demo: 'https://meeting-urusng.vercel.app/',
        image: 'https://i.ibb.co/mFZSD9R/Screen-Shot-2023-09-14-at-13-58-29.png'
    },
    {
        id: 3,
        projectName: 'Chat System',
        projectDesc: 'Coming soon after deploy success',
        tags: ['React', 'Golang', 'Flutter'],
        code: '',
        demo: '',
        image: ''
    },
    {
        id: 4,
        projectName: 'Finance - eWallet',
        projectDesc: 'Coming soon after deploy success',
        tags: ['React', 'Golang', 'Flutter'],
        code: '',
        demo: '',
        image: ''
    },
]