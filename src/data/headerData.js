// import resume from '../assets/pdf/resume.pdf'

export const headerData = {
    name: 'Taric Nguyen',
    title: "Project Owner",
    desciption: "Life may be a meandering path but don't worry about it, rather enjoy it. It is about the journey, not the destination so explore all that comes your way, learn and grow from it, for this journey only happens once.",
    image: 'img_1.png',
    resumePdf: 'https://studentctueteduvn-my.sharepoint.com/:b:/g/personal/ndttri_khmt0115_student_ctuet_edu_vn/EfbKdd-iSXRAqEVJHWfnHBgBwO2SeJNRk9Jnl4DWf0XvMQ?e=yGzpCc'
}